const { Router } = require('express');
const { personasGet, personaPut, personaPost, personaDelete, personasGetByCIoSexo, personasGetByLastNameAndGener } = require('../controllers/personaController');

const router = Router();

router.get('/', personasGet);
router.get('/:Z', personasGetByCIoSexo);
//router.get('/:sexo', personasGetByGenero);
router.get('/:x/:sexo', personasGetByLastNameAndGener);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);

module.exports = router;