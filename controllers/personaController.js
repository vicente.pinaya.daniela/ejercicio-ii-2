const { guardarDB, getDB } = require('../helpers/guardarDB');
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const {request ,  response } = 'express';

const personas = new Personas();
  



const personasGet = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }

  //filtrar por nombre y apellido
  ///api/persona/?nombres=[x]&apellidos=[y] → Buscar por un nombre y apellido
   const {nombres, apellidos} = req.query
   console.log(nombres,apellidos);
   let persona ;
   if (nombres && apellidos) {
      persona = personas.buscarPersona(nombres,apellidos);
      res.json({
        persona
      });
   }else{
      res.json({
        personasDB
      });
   }
  // let array =   personas._listado;
};

///api/persona/CI → Buscar por CI
const personasGetByCIoSexo = (req, res = response) => {
  console.log("holaaaaa");
  const personasDB =  getDB();
  let personaCI;
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  
  const {Z} =req.params
  if (parseInt(Z)) {

   personaCI = personas.buscarCI(Z);
   res.json({
    personaCI
  });
}else{
  
  personaGenero = personas.buscarGenero(Z);
  res.json({
   personaGenero
 });
 }
// let array =   personas._listado;
};


///api/persona/[x]/[sexo] →Buscar por apellidos que comiencen por una letra introducida por teclado x (LIKE)
//y que tengan el sexo masculino o femenino.
const personasGetByLastNameAndGener = (req, res = response) => {
  const personasDB =  getDB();
  let personaX;
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }


  const  {x, sexo} = req.params;

  if (x && sexo) {
    personaX = personas.obtenerFiltro(x, sexo); 
  
    if (personas != []) {
      res.json({
        msg: 'post API - Controlador',
        personaX
      });
    }else{
      res.json({
        msg: 'No existen los datos',
      });
    }
  }
}; 




const personaPost = (req, res = response) => {

 
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
  let personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  personas.crearPersona(persona);
  // console.log(listado);
  guardarDB(personas.listadoArr);
  personasDB =  getDB();
  res.json({
    msg: 'post API - Controlador',
    personasDB
  });
};

const personaPut = (req, res = response) => {
  const {id} = req.params;//LO QUE RECIBE POR LA URL KEY
  
  if (id) {
    personas.borrarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDB(personas.listadoArr);
  }
  res.json({
    msg: 'put API - Controlador',
    
  });

};

//Crear y mandar informacion  con POST:
const personaDelete = (req, res = response) => {

  const {id} = req.params;
  // console.log(id);
  if (id) {
    personas.borrarPersona(id);
    guardarDB(personas.listadoArr);
     //console.log(personas._listado);
  }

  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  personasGet,
  personasGetByCIoSexo,
  //personasGetByGenero,
  personasGetByLastNameAndGener,
  personaPut,
  personaPost,
  personaDelete
}